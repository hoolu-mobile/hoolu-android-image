FROM openjdk:8
MAINTAINER Isah Petr <wcpetr01@@gmail.com>

   
RUN apt-get update -qq

#1. Dependencies to execute Android builds
RUN dpkg --add-architecture i386 
RUN apt-get update -qq 
RUN apt-get update && apt-get -f -qq install -y wget 
RUN apt-get update && apt-get -f -qq install unzip 
RUN apt-get clean 




ENV ANDROID_SDK_VERSION 3859397
ENV ANDROID_HOME /opt/android-sdk-linux

#2 Download Android SDK tools into $ANDROID_HOME

RUN cd /opt \
    && wget -q https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip -O android-sdk-tools.zip \
    && unzip -q android-sdk-tools.zip -d ${ANDROID_HOME} \
    && rm android-sdk-tools.zip

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools

#3. Setup android environment 
ENV TARGET_SDK_VERSION "26"
ENV BUILD_TOOLS_VERSION "26.0.2"

RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager "--licenses"
RUN ${ANDROID_HOME}/tools/bin/sdkmanager  "emulator" "tools" "platform-tools"

# Install SDK Packages
RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager \
    "platforms;android-${TARGET_SDK_VERSION}" \                                                      
    "build-tools;${BUILD_TOOLS_VERSION}" \                            
    "extras;android;m2repository" \
    "extras;google;m2repository" \
    "extras;google;google_play_services" \
    "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" \
    "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.1" \
    "add-ons;addon-google_apis-google-23" 


#Allow the host to use gradle cache, otherwise gradle will always download plugins & artifacts on every build
VOLUME ["/root/.gradle/caches/"]


# Download and Install Gradle
ENV GRADLE_VERSION 4.1

RUN cd /opt && \
    wget -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip && \
    unzip gradle*.zip && \
    rm gradle*.zip

#   ls -d */ | sed 's/\/*$//g' | xargs -I{} mv {} gradle && \

#export and set environment variables
ENV GRADLE_HOME /opt/gradle-4.1/
ENV PATH ${GRADLE_HOME}/bin:$PATH

#6.creating user and  Define working directory.

# Go to workspace
RUN mkdir -p /opt/workspace
WORKDIR /opt/workspace

#ADD build.sh /opt/workspace

# Fix permissions
#RUN chmod 700 $ANDROID_HOME 








