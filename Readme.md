## Android Docker Image
#### Local Development Environment set up
#### Things that you should have done before using the script.
* Before you start

* Pull the image from the repository
```docker pull registry.gitlab.com/hoolu-mobile/hoolu-android-image:latest```

* mount your local host directory 
```docker run -it --privileged --volume=$(pwd)/hoolu-calc-app:/opt/workspace <image-name> bash```

* If you are to use a predownloaded sdk, mount it , mount your local host directory too
```docker run -it --privileged --volume=$(pwd)/sdk:/opt/workspace --volume=$(pwd)/hoolu-calc-app:/opt/workspace <image-name> bash```

* Running app on Android Device you can give a container access to host's USB Android devices.
```docker run -it --privileged -v /dev/bus/usb:/dev/bus/usb --volume=$(pwd)/hoolu-calc-app:/opt/workspace <image-name> bash```

* try to avoid privileged flag, just add necessary capabilities when possible
* --device option allows you to run devices inside the container without the --privileged flag
```docker run -it --device=/dev/ttyUSB0 --volume=$(pwd)/hoolu-calc-app:/opt/workspace <image-name> bash```

* build the app inside the container 
```gradle installDebug```

* To stop and remove container
```docker rm -f <container-name>```

#### Something to note:
* Connect Android device via USB on host first;
* Launch container;
* Disconnect and connect Android device on USB;
* Select OK for "Allow USB debugging" on Android device;
* Now the Android device will show up inside the container (adb devices)


## Android Docker Development Environment














